# Use the official Node.js 16 image as the base image
FROM node:18

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the app to the container
COPY . .

# Expose the port that the app will run on
EXPOSE 5173

# Start the app in development mode
CMD ["npm", "run", "dev"]
