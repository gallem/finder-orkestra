import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import router from "@/router"

interface IMainStore {
    requestSearchByScan: Function,
    searchText: string,
    enforceProcessSearch: boolean,
    reportSearchDone: Function
    reportSearchStart: Function
}

export const useMainStore = defineStore('main', () => {
    let enforceProcessSearch = ref(false)
    let searchText = ref("")

    function requestSearchByScan(textToSearch: string) {
        enforceProcessSearch.value = true
        searchText.value = textToSearch
    }

    function reportSearchDone(){
        searchText.value = ""
    }

    function reportSearchStart(){
        enforceProcessSearch.value = false
    }
      
    watch(enforceProcessSearch,(value, oldValue) => {
        if(!!value){
            router.push("/")
        }else{

        }
    })

    const returnData:IMainStore = {
        requestSearchByScan,
        searchText,
        enforceProcessSearch,
        reportSearchDone,
        reportSearchStart
    }

    return returnData
})
